import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestSupervisorComponent } from './request-supervisor.component';

describe('RequestSupervisorComponent', () => {
  let component: RequestSupervisorComponent;
  let fixture: ComponentFixture<RequestSupervisorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RequestSupervisorComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(RequestSupervisorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
