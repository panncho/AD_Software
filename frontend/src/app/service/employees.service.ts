import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class EmployeesService {

  constructor(
    private http: HttpClient
  ) { }

  
  view(){
    return this.http.get("/api/v1/users/allrequests") // lets see employes

  }

  deleteOne(id: any){
    const route = "/api/v1/users/" + id
    return this.http.delete(route, id)
  }

}
