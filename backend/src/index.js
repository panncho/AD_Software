const express = require("express");
const v1usersRouter = require("./v1/routes/userRoutes");
var body_parser = require("body-parser");

const app = express();
const PORT = process.env.PORT || 3000;


app.use(express.json())
app.use("/api/v1/users", v1usersRouter)


app.listen(PORT,()=>{
        console.log(`server listening on port ${PORT}`) //fix
    });




