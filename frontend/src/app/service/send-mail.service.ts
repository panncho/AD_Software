import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SendMailService {

  constructor(private http: HttpClient) { }

  sendMail(status_: boolean, mail_: any): Observable<any> {
    const data= {status: status_, mail: mail_}
    console.log(data)
    
    return this.http.post("/api/v1/users/sendmail", data) // solicita correo (accept or rejected, mail)

  }

  
}
