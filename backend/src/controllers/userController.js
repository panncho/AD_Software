const userService= require("../services/userService");
const nodeMailer = require('nodemailer');



enviarMail = async(texto, mail)=>{

    config= {
        host: 'smtp.gmail.com',
        port : 587,
        auth : {
            user: 'franciscoazuniga19@gmail.com',
            pass: 'debaeyklfyouiidg'
        }
    }

    const mensaje = {
        from: 'franciscoazuniga19@gmail.com',
        to: mail,
        subject: 'Solicitud de vacaciones',
        text: texto
    }

    const transport = nodeMailer.createTransport(config);

    const info = await transport.sendMail(mensaje)
    
    console.log(info)

}


const getAllusers = (req, res)=>{
    const Allusers = userService.getAllusers();
    res.send({ status: "OK", data: Allusers}); 
};

const send_mail = (req, res)=>{
    const body = req.body;
    //console.log(body);
    if(
        body.status == null||
        body.mail == null
       
    ){
        return res.send("Datos insuficientes");

    }
    

    try{
        if(body.status == true){  
            console.log(body.status, body.mail) //enviar correo de aprueba
            enviarMail('Junto con saludar nos gustaria informar que su solicitud de vacaciones ha sido aprobada:)', body.mail)
    

            
        }
        if(body.status == false){
            enviarMail('Junto con saludar lamento informar que su solicitud de vacaciones ha sido rechazada, aconsejamos comunicarse con RRHH', body.mail)
    
            //console.log(body.status) //enviar correo de rechaza

        }
    
        res.status(201).send({status: "OK"});

    }catch(error){
        res.status(500).send({status:failed})
    }
};

const getOneuser  = (req, res)=>{
    const {params: {userId},
    } = req;

    if(!userId){
        return;
    }

    const user = userService.getOneuser(userId);
    res.send({status:"OK", data: user});


};

const createNewuser  = (req, res)=>{
    const body = req.body;
    
    if(
        !body.name ||
        !body.mail || 
        !body.id 
    //    !body.excercises  not include
       
    ){
        return res.status(FAILED).send("Datos insuficientes");

    }
    const newuser = {
        id:body.id,
        name: body.name,
        mail: body.mode,
    

    };

    try{
        const createduser = userService.createNewuser(newuser);
        res.status(201).send({status: "OK", data: createduser});

    }catch(error){
        res.status(500).send({status:failed})
    }
    
};

const updateOneuser  = (req, res)=>{
    const {body, params: id}=  req; //req.body
    if(!id){
        res.status(400)
        res.send({status:"FAILED"});
        return;
    }
    const updateduser = userService.updateOneuser(id.id, body);
    res.send({status:"OK", data: updateduser});
};

const deleteOneuser  = (req, res)=>{
    const{
        params: {id}
    } = req;
    console.log(id)
    if(!id){
        res.status(400)
        res.send({status:"FAILED"});
        return
    }

    userService.deleteOneuser(id);
    res.status(204).send({status:"OK"})
};

module.exports={
    getAllusers,
    send_mail,
    getOneuser,
    createNewuser,
    updateOneuser,
    deleteOneuser,
};