const fs = require("fs");

const savetodatabase = (db) =>{
    fs.writeFileSync("./src/db/db.json", JSON.stringify(db,null,2),{
        encoding: "utf-8",
    })
}

module.exports= {savetodatabase};