import { HttpClient } from '@angular/common/http';
import { EmployeesService } from './../../service/employees.service';
import { Component, OnInit } from '@angular/core';
import { SendMailService } from 'src/app/service/send-mail.service';

@Component({
  selector: 'app-request-supervisor',
  templateUrl: './request-supervisor.component.html',
  styleUrls: ['./request-supervisor.component.css']
})
export class RequestSupervisorComponent implements OnInit{
  usuarios: any;
  response: any;
  response2: any;
  response3: any;


 

  constructor(
    private employeesService: EmployeesService,
    private mailSender: SendMailService,

    private http: HttpClient,

  

  ){}

  ngOnInit(): void {
    this.view()
  
  }


  view(){
  
    this.employeesService.view().  

      subscribe(
        data => {
          this.usuarios=data;
        }

        
  )
}
  function_mail_true(mail: any, id: any){
    const ok= true
    this.mailSender.sendMail(ok, mail) .subscribe(
      
      data => {

        this.response= data 

      
      }, (err)=>{
      console.log(err);

      
    })
    
    this.employeesService.deleteOne(id) .subscribe(
      
      data => {

        this.response= data 

      
      }, (err)=>{
      console.log(err);

      
    })
  
    
    
      
  }

  function_mail_false(mail: any, id: any){
    const nook= false
    this.mailSender.sendMail(nook, mail).subscribe(
      
      data => {

        this.response= data 

      
      }, (err)=>{
      console.log(err);
      
    });
    
    this.employeesService.deleteOne(id) .subscribe(
      
      data => {

        this.response= data 

      
      }, (err)=>{
      console.log(err);

      
    })
    

  
      
    
  }
}


