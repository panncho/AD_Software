import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RequestSupervisorComponent } from './components/request-supervisor/request-supervisor.component';
import { InicioComponent } from './components/inicio/inicio.component';
import { SuccessComponent } from './components/success/success.component';

const routes: Routes = [
  
  {
    path: 'requests',
    component:  RequestSupervisorComponent,
    pathMatch: "full"
  },
  {
    path: 'inicio',
    component:  InicioComponent,
    pathMatch: "full"
  },
  {
    path: 'success',
    component:  SuccessComponent,
    pathMatch: "full"
  },
    
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
