const express = require("express");
const router = express.Router();
const userController = require("../../controllers/userController");

router


    .get("/allrequests", userController.getAllusers)

    .get("/:userId", userController.getOneuser)

    .post("/", userController.createNewuser)

    .post("/sendmail", userController.send_mail)

    .patch("/:id", userController.updateOneuser)

    .delete("/:id", userController.deleteOneuser);

module.exports= router;

