# Grupo "Escuadron Tortilla"

Este es el repositorio del grupo "Escuadron Tortilla", cuyos integrantes son: 

- Agustín López. - Rol: 202130549-1
- Francisco Zuñiga. - Rol: 202130509-2
- Francisco Castillo. - Rol: 202130514-9
- **Tutor** : Felipe Marquez. 

## Wiki 

Puedes acceder a la Wiki mediante el siguiente [enlace](https://gitlab.com/panncho/AD_Software/-/wikis/home)


## Videos 

[Video Hito 1](https://www.youtube.com/watch?v=lgh6zMJfQDI)
<br>
[Video Hito 4](https://youtu.be/L2gkrWjOGVM)
<br>
[Video Hito 6 y 7](https://youtu.be/-rgBTb6y4x0)


## Presentacion.

[Presentacion 1](https://www.canva.com/design/DAFeU_Zkukc/mBBjCPH0brsbnlQm16RO8Q/edit?utm_content=DAFeU_Zkukc&utm_campaign=designshare&utm_medium=link2&utm_source=sharebutton)
