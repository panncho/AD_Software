const db = require("./db.json")
const {savetodatabase}= require("./utils")

const getAllusers= ()=>{
    return db.users;
}

const getOneuser= (usersId)=>{
    const user = db.users.find((user)=>user.id === userId);
    // no se compueba existencia
    return user;
}

const createdUser = (newuser) =>{
    const isalreadyadded = db.users.findIndex(users=> user.name === newuser.name) 

        if (isalreadyadded){
            throw{
                status: 400,
                message: "ya existe"
            }
            

        }

        try{
            db.users.push(newuser);
            savetodatabase(db);


        }catch(error){
            throw{status: 500, message: error}

        }
   
}

const updateOneuser=(userId, changes)=>{
    const indexforupdated = db.users.findIndex((user)=>  (user.id = userId)
    )
    if (indexforupdated === -1){
        return;
    }
    const updateduser = {
        ...db.users[indexforupdated],
        ...changes,
        updatedat: new Date().toLocaleDateString("en-US", {timezone: "UTC"}),
    }
    db.users[indexforupdated] = updateduser;
    savetodatabase(db);
    
    return updateduser;

    
}

const deleteOneuser = (userId)=>{
    const indexfordeleted = db.users.findIndex((user)=>  (user.id = userId)
    )

    if (indexfordeleted === -1){
        
        return;
    }
    
    db.users.splice(indexfordeleted,1);
    savetodatabase(db);
};



module.exports = {getAllusers, createdUser, getOneuser, updateOneuser, deleteOneuser};